package mvc.employee.model.view;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import mvc.employee.model.Department;

public class EmployeeEditController {
	
	@FXML
	private ComboBox<Department> cbDepartments;
	
	
	public void setDepartments(ObservableList<Department> olDepartment) {
		cbDepartments.getItems().clear();
		cbDepartments.setItems(olDepartment);		
	}
	
	@FXML
	public void saveEmployee() {
		
	}
	
	@FXML
    public void cancelEmployee() {
		
	}

}
