package mvc.employee.model.view;

import java.time.LocalDate;

import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mvc.employee.model.Department;
import mvc.employee.model.Employee;

public class EmployeeController {

	// TableView , TableColumn
	@FXML
	private TableView<Employee> employeeTable;
	@FXML
	private TableColumn<Employee, Integer> employeeIdColumn;
	@FXML
	private TableColumn<Employee, String> firstNameColumn;
	@FXML
	private TableColumn<Employee, String> lastNameColumn;
	@FXML
	private TableColumn<Employee, String> emailColumn;
	@FXML
	private TableColumn<Employee, String> phoneColumn;
	@FXML
	private TableColumn<Employee, LocalDate> hireDateColumn;
	@FXML
	private TableColumn<Employee, String> hireDateAsStrColumn;
	@FXML
	private TableColumn<Employee, String> jobIdColumn;
	@FXML
	private TableColumn<Employee, Integer> salaryColumn;
	@FXML
	private TableColumn<Employee, Integer> managerIdColumn;
	@FXML
	private TableColumn<Employee, Integer> departmentIdColumn;
	
	@FXML
	private TableColumn<Employee, String> departmentNameColumn;
	@FXML
	private TableColumn<Employee, String> managerNameColumn;
	@FXML
	private TableColumn<Employee, String> jobTitleColumn;
	
	


	// Label
	@FXML
	private Label employeeIdLabel;
	@FXML
	private Label firstNameLabel;
	@FXML
	private Label lastNameLabel;
	@FXML
	private Label emailLabel;
	@FXML
	private Label phoneLabel;
	@FXML
	private Label hireDateLabel;
	@FXML
	private Label jobIdLabel;
	@FXML
	private Label salaryLabel;
	@FXML
	private Label managerIdLabel;
	@FXML
	private Label departmentIdLabel;

	@FXML
	private void initialize() {
		employeeTable.setTableMenuButtonVisible(true);
		employeeIdColumn.setCellValueFactory(cellData -> cellData.getValue().employeeIdProperty().asObject());
		firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
		lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
		emailColumn.setCellValueFactory(cellData -> cellData.getValue().emailProperty());
		phoneColumn.setCellValueFactory(cellData -> cellData.getValue().phoneProperty());
		hireDateColumn.setCellValueFactory(cellData -> cellData.getValue().hireDateProperty());
		jobIdColumn.setCellValueFactory(cellData -> cellData.getValue().jobIdProperty());
		salaryColumn.setCellValueFactory(cellData -> cellData.getValue().salaryProperty().asObject());
		managerIdColumn.setCellValueFactory(cellData -> cellData.getValue().managerIdProperty().asObject());
		departmentIdColumn.setCellValueFactory(cellData -> cellData.getValue().departmentIdProperty().asObject());

		// ustaw wartości pól
		refreshEmployee(null);
		// słuchaj zmiany zaznaczonego wiersza
		employeeTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> refreshEmployee(newValue));
	}

	public void setEmployees(ObservableList<Employee> olEmployees) {
		employeeTable.getItems().clear();
		employeeTable.setItems(olEmployees);

		if (!employeeTable.getItems().isEmpty())
			employeeTable.getSelectionModel().select(0);
	}	

	private void refreshEmployee(Employee emp) {
		if (emp != null) {
			employeeIdLabel.setText(Integer.toString(emp.getEmployeeId()));
			firstNameLabel.setText(emp.getFirstName());
			lastNameLabel.setText(emp.getLastName());
			emailLabel.setText(emp.getEmail());
			phoneLabel.setText(emp.getPhone());
			hireDateLabel.setText(emp.getHireDate().toString());
			jobIdLabel.setText(emp.getJobId());
			salaryLabel.setText(Integer.toString(emp.getSalary()));
			managerIdLabel.setText(Integer.toString(emp.getManagerId()));
			departmentIdLabel.setText(Integer.toString(emp.getDepartmentId()));
				} else {
			employeeIdLabel.setText("");
			firstNameLabel.setText("");
			lastNameLabel.setText("");
			emailLabel.setText("");
			phoneLabel.setText("");
			hireDateLabel.setText("");
			jobIdLabel.setText("");
			salaryLabel.setText("");
			managerIdLabel.setText("");
			departmentIdLabel.setText("");
		}
	}
	
	@FXML
	public void deleteEmployee() {
		
	}
	
	@FXML
    public void editEmployee() {
		
	}
    
    @FXML
    public void addEmployee() {
		
	}

}
