package mvc.employee;

import java.util.Optional;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import mvc.employee.model.Employee;
import mvc.employee.model.dal.DepartmentsDAL;
import mvc.employee.model.dal.EmployeesDAL;
import mvc.employee.model.dal.OraConn;
import mvc.employee.model.view.AlertBox;
import mvc.employee.model.view.EmployeeController;
import mvc.employee.model.view.MainController;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		if (OraDbConnect() > 0) {
			return;
		}
		try {

			ViewLoader<BorderPane, Object> viewLoader = new ViewLoader<BorderPane, Object>("model/view/Main.fxml");
			BorderPane borderPane = viewLoader.getLayout();
			Scene scene = new Scene(borderPane);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Pracownicy");
			
			ViewLoader<AnchorPane, EmployeeController> viewLoaderEmp = new ViewLoader<AnchorPane, EmployeeController>("model/view/EmployeeData.fxml");
			AnchorPane anchorPaneEmp = viewLoaderEmp.getLayout();
			borderPane.setCenter(anchorPaneEmp);
			((MainController) viewLoader.getController()).setStage(primaryStage);
			EmployeeController empControler = viewLoaderEmp.getController();
			((MainController) viewLoader.getController()).setStage(primaryStage);
			((MainController) viewLoader.getController()).setEmployeeFXML(viewLoaderEmp);

			ObservableList<Employee> emps = new EmployeesDAL().getEmployees();
			empControler.setEmployees(emps);			

			primaryStage.setOnHiding(e -> primaryStage_Hiding(e));
			primaryStage.setOnCloseRequest(e -> primaryStage_CloseRequest(e));
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	int OraDbConnect() {
		int ret = OraConn.open("jdbc:oracle:thin:@ora3.elka.pw.edu.pl:" + "1521:ora3inf", "xtemp16", "xtemp16");

		if (ret > 0) {
			AlertBox.showAndWait(AlertType.ERROR, "Nawiązanie połączenia z bazą danych",
					"Nieprawidłowy użytkownik lub hasło.\n");
		}
		return ret;
	}

	void primaryStage_Hiding(WindowEvent e) {
		try {
			OraConn.close();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	void primaryStage_CloseRequest(WindowEvent e) {
		Optional<ButtonType> result = AlertBox.showAndWait(AlertType.CONFIRMATION, "Kończenie	pracy	",
				"	Czy	chcesz	zamknąć	aplikację	?");
		if (result.orElse(ButtonType.CANCEL) != ButtonType.OK)
			e.consume();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
